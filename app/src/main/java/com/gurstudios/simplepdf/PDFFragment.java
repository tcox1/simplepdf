package com.gurstudios.simplepdf;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Gur814 on 1/23/15.
 */
public class PDFFragment extends Fragment {

    private TextView pdfName;

    private RelativeLayout pdfOuter;
    private FrameLayout pdfContainer;
    private PDFView pdfView;
    private View panel;
    private View touchOutside;

    private ImageButton back;
    private ImageButton forward;

    private TextView pageView;
    private int currentPage;
    private int pageCount;

    public static PDFFragment getInstance(int defaultIndex, ArrayList<String> names, ArrayList<String> files, ArrayList<String> images) {
        PDFFragment fragment = new PDFFragment();
        Bundle args = new Bundle();
        args.putInt(MainActivity.DEFAULT_KEY, defaultIndex);
        args.putStringArrayList(MainActivity.NAMES_KEY, names);
        args.putStringArrayList(MainActivity.FILES_KEY, files);
        args.putStringArrayList(MainActivity.IMAGES_KEY, images);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.pdf_fragment, container, false);

        Bundle arguments = getArguments();
        ArrayList<String> names = arguments.getStringArrayList(MainActivity.NAMES_KEY);
        ArrayList<String> files = arguments.getStringArrayList(MainActivity.FILES_KEY);
        ArrayList<String> images = arguments.getStringArrayList(MainActivity.IMAGES_KEY);

        touchOutside = root.findViewById(R.id.touch_outside);
        pdfOuter = (RelativeLayout) root.findViewById(R.id.pdf_outer);
        pdfContainer = (FrameLayout) root.findViewById(R.id.pdf_container);
        pdfName = (TextView) root.findViewById(R.id.name_text_view);
        setupPDFView(root);
        setupPanel(root, names, files, images);

        touchOutside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (touchOutside.getVisibility() == View.VISIBLE)
                    hidePanel();
            }
        });

        int defaultIndex = arguments.getInt(MainActivity.DEFAULT_KEY, 0);
        if (defaultIndex >= 0 && defaultIndex < files.size())
            loadFile(names.get(defaultIndex), files.get(defaultIndex));

        return root;
    }

    private void setupPanel(View root, ArrayList<String> names, ArrayList<String> files, ArrayList<String> images) {
        View topBar = root.findViewById(R.id.top_bar);
        panel = root.findViewById(R.id.panel);
        topBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation slideDown = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
                panel.setVisibility(View.VISIBLE);
                panel.startAnimation(slideDown);

                Animation scale = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_down);
                scale.setFillAfter(true);
                pdfOuter.startAnimation(scale);

                Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                fadeIn.setFillAfter(true);
                touchOutside.startAnimation(fadeIn);
                touchOutside.setVisibility(View.VISIBLE);
            }
        });

        RecyclerView pdfList = (RecyclerView) root.findViewById(R.id.pdf_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        pdfList.setLayoutManager(layoutManager);
        pdfList.setAdapter(new FileAdapter(this, names, files, images));
    }

    private void setupPDFView(View root) {
        pdfView = (PDFView) root.findViewById(R.id.pdfview);
        Button done = (Button) root.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        back = (ImageButton) root.findViewById(R.id.pdf_back);
        forward = (ImageButton) root.findViewById(R.id.pdf_forward);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPage > 1) {
                    pdfView.jumpTo(currentPage - 1);
                }
            }
        });

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPage != pageCount) {
                    pdfView.jumpTo(currentPage + 1);
                }
            }
        });

        pageView = (TextView) root.findViewById(R.id.page_view);
    }

    private void hidePanel() {
        Animation slideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        slideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                panel.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        panel.startAnimation(slideUp);

        Animation scaleUp = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_up);
        scaleUp.setFillAfter(true);
        pdfOuter.startAnimation(scaleUp);

        Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        fadeOut.setFillAfter(true);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                touchOutside.setVisibility(View.INVISIBLE);
                touchOutside.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        touchOutside.startAnimation(fadeOut);
    }

    public void fileClicked(String name, String file) {
        loadFile(name, file);
        hidePanel();
    }

    private void loadFile(String name, String file) {
        pdfName.setText(name);

        // Create a new PDFView. This is a work around for a bug that crashes the app if a PDF
        // is displayed before the previous has finished rendering its current page.
        pdfView = new PDFView(getActivity(), null);
        pdfContainer.removeAllViews();
        pdfContainer.addView(pdfView);

        pdfView.fromFile(new File(file))
                .showMinimap(false)
                .enableSwipe(true)
                .onPageChange(new OnPageChangeListener() {
                    @Override
                    public void onPageChanged(int page, int pageCount) {
                        currentPage = page;
                        PDFFragment.this.pageCount = pageCount;

                        if (currentPage == 1)
                            back.setVisibility(View.INVISIBLE);
                        else
                            back.setVisibility(View.VISIBLE);

                        if (currentPage == pageCount)
                            forward.setVisibility(View.INVISIBLE);
                        else
                            forward.setVisibility(View.VISIBLE);

                        pageView.setText("Page " + currentPage + "/" + pageCount);
                    }
                })
                .load();
        currentPage = 1;
        pageCount = pdfView.getPageCount();
    }

}
