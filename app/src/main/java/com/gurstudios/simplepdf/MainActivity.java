package com.gurstudios.simplepdf;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    public static final String TAG = "SimplePDF";

    public static final String NAMES_KEY = "names";
    public static final String FILES_KEY = "files";
    public static final String IMAGES_KEY = "images";
    public static final String DEFAULT_KEY = "default";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> names = getIntent().getStringArrayListExtra("names");
        ArrayList<String> files = getIntent().getStringArrayListExtra("files");
        ArrayList<String> images = getIntent().getStringArrayListExtra("images");
        int defaultIndex = getIntent().getIntExtra(DEFAULT_KEY, 0);

        if (names == null || files == null || images == null) {
            finish();
            return;
        }

//        names = new ArrayList<>();
//        files = new ArrayList<>();
//        images = new ArrayList<>();
//
//        names.add("Test 1");
//        names.add("Test 2");
//        names.add("Test 3");
//
//        files.add("/data/data/com.tuch.view/files/testpdf.pdf");
//        files.add("/data/data/com.tuch.view/files/testpdf2.pdf");
//        files.add("/data/data/com.tuch.view/files/testpdf3.pdf");
//
//        images.add("/data/data/com.tuch.view/files/19f5c565d5d7194bd64c66331ec354c8");
//        images.add("/data/data/com.tuch.view/files/19f5c565d5d7194bd64c66331ec354c8");
//        images.add("/data/data/com.tuch.view/files/19f5c565d5d7194bd64c66331ec354c8");

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, PDFFragment.getInstance(defaultIndex, names, files, images)).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.pdf_fragment, container, false);
            return rootView;
        }
    }
}
