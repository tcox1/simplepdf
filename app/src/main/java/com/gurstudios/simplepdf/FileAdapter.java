package com.gurstudios.simplepdf;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Gur814 on 1/27/15.
 */
public class FileAdapter extends RecyclerView.Adapter<FileAdapter.FileHolder> {

    private PDFFragment fragment;
    private LayoutInflater inflater;
    private ArrayList<String> names;
    private ArrayList<String> files;
    private ArrayList<String> images;

    public FileAdapter(PDFFragment fragment, ArrayList<String> names, ArrayList<String> files, ArrayList<String> images) {
        inflater = (LayoutInflater) fragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragment = fragment;
        this.names = names;
        this.files = files;
        this.images = images;
    }

    @Override
    public FileHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new FileHolder(inflater.inflate(R.layout.pdf_file_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(final FileHolder fileHolder, final int position) {
        fileHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment.fileClicked(fileHolder.name, fileHolder.file);
            }
        });
        fileHolder.setData(names.get(position), files.get(position), images.get(position));
    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    class FileHolder extends RecyclerView.ViewHolder {

        String name;
        String file;
        String image;

        View outer;
        ImageView imageView;
        TextView nameView;

        public FileHolder(View itemView) {
            super(itemView);
            this.outer = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.image);
            nameView = (TextView) itemView.findViewById(R.id.name);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            this.outer.setOnClickListener(listener);
        }

        public void setData(String name, String file, String image) {
            // Set data.
            this.name = name;
            this.file = file;
            this.image = image;

            // Load data into views.
            this.nameView.setText(name);

            if (image != null)
                Picasso.with(fragment.getActivity()).load(new File(image)).fit().centerCrop().error(R.drawable.ic_file_document_box_white_48dp).into(imageView);
            else
                Picasso.with(fragment.getActivity()).load(R.drawable.ic_file_document_box_white_48dp).into(imageView);
        }

    }

}
